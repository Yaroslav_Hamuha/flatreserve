<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>flatReserve</title>
</head>
<body>
<p>
<h2 style="color:#ff0000">  ${name} </h2>
<h2 style="color:#800000">  ${name2} </h2>
</p>
<h2>  front-end form for data transmit to server-side Servlet used GET HTTP protocol </h2>

<p>
  	<form action="/addtenant" method="post">
	<button type="submit">addTenant</button>
	   <input name="name" type="text" size="20" placeholder="name">
	   <input name="phone" type="text" size="20" placeholder="phone">
	   <input name="email" type="text" size="20" placeholder="email">
	</form>
</p> 
<p>
  	<form action="/addapartment" method="post">
	<button type="submit">addApartment</button>
	   <input name="landlord_id" type="text" size="20" placeholder="landlord_id">
	   <input name="address" type="text" size="20" placeholder="address">
	   <input name="price" type="text" size="20" placeholder="price">
	   <input name="period" type="text" size="20" placeholder="period">
	   <input name="status" type="text" size="20" placeholder="status">
	</form>
</p> 
<p>
  	<form action="/deleteapartment" method="post">
	<button type="submit">deleteApartment</button>
	   <input name="id" type="text" size="20" placeholder="id">
	</form>
</p> 
<p>
  	<form action="/addlandlord" method="post">
	<button type="submit">addLandlord</button>
	   <input name="name" type="text" size="20" placeholder="name">
	   <input name="phone" type="text" size="20" placeholder="phone">
	   <input name="email" type="text" size="20" placeholder="email">
	</form>
</p> 
<p>
  	<form action="/reserveapartment" method="post">
	<button type="submit">reserveApartment</button>
		<input name="landlords_id" type="text" size="20" placeholder="landlords_id">
		<input name="tenants_id" type="text" size="20" placeholder="tenants_id">
		<input name="period" type="text" size="20" placeholder="period">
	    <input name="id" type="text" size="20" placeholder="id">
	    <input name="rent_period" type="text" size="20" placeholder="rent_period">
	    <input name="apartments_id" type="text" size="20" placeholder="apartments_id">
	</form>
</p> 

</body>
</html>
