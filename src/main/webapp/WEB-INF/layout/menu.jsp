<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="navigation">
  <ul>
    <li>
      <a href="\index.jsp" title="Welcome Page">Welcome Page</a>
    </li>
    <li>
      <a href="\landlords.jsp" title="Landlords">Landlords</a>
    </li>
    <li>
      <a href="\tenants.jsp" title="Tenants">Tenants</a>
    </li>
    <li>
      <a href="\apartments.jsp" title="Apartments">Apartments</a>
    </li>
  </ul>
</div>