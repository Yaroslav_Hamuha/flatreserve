<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<style type="text/css">
  tr, td{
    border: 1px solid black;
  }
  td{
    width: 150px;
  }
  td#body{
    width: 400px;
  }
</style>
<table>
  <tr>
    <td colspan="2">
      <tiles:insertAttribute name="header" />
    </td>
  </tr>
  <tr>
    <td>
      <tiles:insertAttribute name="menu" />
    </td>
    <td id="body">
      <tiles:insertAttribute name="body" />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <tiles:insertAttribute name="footer" />
    </td>
  </tr>
</table>