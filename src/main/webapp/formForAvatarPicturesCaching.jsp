<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>File Upload</title>
</head>
<body>
    <div style="text-align:center">
        <h1>File Upload</h1>
        <form method="post" action="avatarpicturescaching" enctype="multipart/form-data">
            Select file to upload: <input type="file" name="avatarFileName" size="160" /><br />
            <br /> <input type="submit" value="upload" />
        </form>
    </div>
</body>
</html>