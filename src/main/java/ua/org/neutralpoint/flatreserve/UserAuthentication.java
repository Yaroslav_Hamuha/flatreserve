package ua.org.neutralpoint.flatreserve;

public class UserAuthentication {
    private String name = "UNKNOWN";
    private String password = "UNKNOWN";

    public UserAuthentication(String name, String password){
        this.name = name;
        this.password = password;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
