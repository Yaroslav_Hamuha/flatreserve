package ua.org.neutralpoint.flatreserve.dao;

public interface ApartmentDao {

    public void addApartment (int landlord_id, String address, int price, String period, String status) throws Exception;

    public void deleteApartment (int id) throws Exception;

    public void reserveApartment (int landlords_id, int tenants_id, String period, int id,
                                  String rent_period, int apartment_id) throws Exception;


    
}
