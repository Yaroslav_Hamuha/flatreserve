package ua.org.neutralpoint.flatreserve.dao;

public interface LandlordDao {

   public void addLandlord (String name, String phone, String email) throws Exception;

}
