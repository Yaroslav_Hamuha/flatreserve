package ua.org.neutralpoint.flatreserve.dao.impl;

import ua.org.neutralpoint.flatreserve.dao.JdbcConnection;
import ua.org.neutralpoint.flatreserve.dao.TenantDao;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TenantDaoJDBCImpl implements TenantDao {

    Logger log = LogManager.getLogger(AccountDaoJDBCImpl.class.getName());

    public TenantDaoJDBCImpl() {
    }

    public void addTenant(String name, String phone, String email) throws Exception {

        String sql = "INSERT INTO tenants (name, phone, email) " +
                "VALUES (?, ?, ?)";

        Connection conn = null;
        PreparedStatement ps = null;

        try(JdbcConnection connection = new JdbcConnection();){
            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, phone);
            ps.setString(3, email);
            // execute insert SQL statement
            ps.executeUpdate();
        }
         catch (SQLException e) {
            log.error("addTenant connection and transaction error"+e);
        }
    }
}




