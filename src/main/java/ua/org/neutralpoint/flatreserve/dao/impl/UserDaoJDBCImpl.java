package ua.org.neutralpoint.flatreserve.dao.impl;

import ua.org.neutralpoint.flatreserve.dao.JdbcConnection;
import ua.org.neutralpoint.flatreserve.dao.UserDao;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserDaoJDBCImpl implements UserDao {

    Logger log = LogManager.getLogger(AccountDaoJDBCImpl.class.getName());

    public UserDaoJDBCImpl() {
    }

    @Override
    public ResultSet GetUserInfo(String id) throws Exception{

        Connection conn = null;
        java.sql.PreparedStatement ps = null;
        String sql = "select * from accounts where id=?";
        ResultSet rs = null;

        try (JdbcConnection connection = new JdbcConnection();){
            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
        }
        catch (SQLException e) {
            log.error("GetUserInfo conn and transaction error"+e);
        }
        return rs;
    }

    @Override
    public void addAvatarUrlPath(String avatar_url_path) throws Exception {
        PreparedStatement preparedStatement = null;

        // useful field - avatar_url_path,
        // others field - for test

        String sql = "INSERT INTO users (username, password, avatar, end_update, avatar_url_path) " +
                "VALUES (?,?,?,?,?)";

        Connection conn = null;
        PreparedStatement ps = null;

        // tesf fields:
        String username ="Artem";
        String password = "1234";


        try(JdbcConnection connection = new JdbcConnection();){
            // test File class and InputStream class for blob field
            File file = new File("C://userImages//av_test.jpg");
            FileInputStream fis = new FileInputStream(file);
            // test data for Time field
            java.util.Date today = new java.util.Date();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());

            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setBlob(3, fis);
            ps.setTimestamp(4, timestamp);
            ps.setString(5,avatar_url_path);

            ps.executeUpdate();
        }
        catch (SQLException e) {
            log.error("addAvatarUrlPath UserDaoJDBCImpl error"+e);
        }
}
    @Override
    public void addAvatarPicture(String avatar_url_path) throws Exception {
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO users (username, password, avatar, end_update, avatar_url_path) " +
                "VALUES (?,?,?,?,?)";

        Connection conn = null;
        PreparedStatement ps = null;

        // test data are filling "username" :
        String username ="Alexei";
        // test data are filling "username" :
        String password = "5678";

        try(JdbcConnection connection = new JdbcConnection();){

            File file = new File(avatar_url_path);
            FileInputStream fis = new FileInputStream(file);

            java.util.Date today = new java.util.Date();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());

            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setBlob(3, fis);
            ps.setTimestamp(4, timestamp);
            ps.setString(5,avatar_url_path);

            ps.executeUpdate();
        }
        catch (SQLException e) {
            log.error("addAvatarUrlPath UserDaoJDBCImpl error"+e);
        }
    }
    @Override
    public void addUsernameAndPassword(String username, String password) throws Exception {
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO users (username, password, avatar, end_update, avatar_url_path) " +
                "VALUES (?,?,?,?,?)";

        Connection conn = null;
        PreparedStatement ps = null;

        // test data are filling "avatar" :
        File file = new File("c:\\temp\\test.txt");
        FileInputStream fis = new FileInputStream(file);
        // test data are filling "end_update" :
        java.util.Date today = new java.util.Date();
        java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());
        // test data are filling "avatar_url_path" :
        String avatar_url_path = "c:\\temp\\test\\";


        try(JdbcConnection connection = new JdbcConnection();){

            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setBlob(3, fis);
            ps.setTimestamp(4, timestamp);
            ps.setString(5,avatar_url_path);

            ps.executeUpdate();
        }
        catch (SQLException e) {
            log.error("addAvatarUrlPath UserDaoJDBCImpl error"+e);
        }
    }




}





