package ua.org.neutralpoint.flatreserve.dao.impl;

import ua.org.neutralpoint.flatreserve.dao.JdbcConnection;
import ua.org.neutralpoint.flatreserve.dao.ApartmentDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApartmentDaoJDBCImpl implements ApartmentDao  {

    Logger log = LogManager.getLogger(ApartmentDaoJDBCImpl.class.getName());

    public void addApartment(int landlord_id, String address, int price, String period, String status) throws Exception {

        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO apartments (landlord_id, address, price, period, status) " +
                "VALUES (?, ?, ?, ?, ?)";

        Connection conn = null;
        PreparedStatement ps = null;

        try(JdbcConnection connection = new JdbcConnection();){
            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, landlord_id);
            ps.setString(2, address);
            ps.setInt(3, price);
            ps.setString(4, period);
            ps.setString(5, status);

            //        Data doesn't save into the table, because
            //        can't save id parameter which set as autoincrement mode.
            ps.executeUpdate();
        }
        catch (SQLException e) {
            log.error("addApartment transaction error"+e);
        }
    }

    public void deleteApartment(int id) throws Exception {

        PreparedStatement preparedStatement = null;

        String sql = "DELETE FROM apartments WHERE id=? AND status='Free'";

        Connection conn = null;
        PreparedStatement ps = null;
        try (JdbcConnection connection = new JdbcConnection();){
            conn = connection.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        }
         catch (SQLException e) {
            log.error("deleteApartment transaction error"+e);
        }
    }
    public void reserveApartment(int landlords_id, int tenants_id, String period, int id,
                                 String rent_period, int apartment_id) throws Exception {
        Connection conn = null;

        PreparedStatement updateApartments = null;
        PreparedStatement updateTransactions = null;



            String sqlForApartmentsUpdate =
                    "UPDATE apartments SET period = ?, status = 'Reserved' WHERE id=?";
            String sqlForTransactionsUpdate =
                    "UPDATE transactions SET landlords_id =?, tenants_id=?, rent_period = ? WHERE apartments_id=?";
        try (JdbcConnection connection = new JdbcConnection();)
            {
                updateTransactions = conn.prepareStatement(sqlForTransactionsUpdate);
                updateApartments = conn.prepareStatement(sqlForApartmentsUpdate);
                conn = connection.getConnection();
                conn.setAutoCommit(false);
                updateApartments.setString(1, period);
                updateApartments.setInt(2, id);
                updateApartments.executeUpdate();
                updateTransactions.setInt(1, id);
                updateTransactions.setInt(2, id);
                updateTransactions.setString(3, rent_period);
                updateTransactions.setInt(4, apartment_id);
                updateTransactions.executeUpdate();
                conn.commit();
            } catch (SQLException e) {
                log.error("reserveApartment transaction error"+e);
                if (conn != null) {
                    try {
                        log.error("Transaction is being rolled back" + e);
                        conn.rollback();
                    } catch (SQLException e1) {
                        log.error("reserveApartment rollback transaction error"+e1);
                    }
            }
    }
}}
