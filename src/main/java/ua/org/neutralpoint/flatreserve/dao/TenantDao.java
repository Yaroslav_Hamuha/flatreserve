package ua.org.neutralpoint.flatreserve.dao;


public interface TenantDao {

    public void addTenant(String name, String phone, String email) throws Exception;

}
