package ua.org.neutralpoint.flatreserve.dao;

import com.mysql.fabric.jdbc.FabricMySQLDriver;
import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JdbcConnection implements AutoCloseable{

    Logger log = LogManager.getLogger(JdbcConnection.class.getName());


    private static final String URL = "jdbc:mysql://localhost:3306/flat_reserve";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private Connection connection;

    public JdbcConnection() {

        try {
            //Driver driver = new FabricMySQLDriver();
            //com.mysql.jdbc.Driver
            try {
                Class.forName ("com.mysql.jdbc.Driver").newInstance ();
            } catch (InstantiationException|IllegalAccessException|ClassNotFoundException  e) {
                e.printStackTrace();
            }
            //DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        } catch (SQLException e) {
            log.error("Database connection error"+e);

        }

    }

    public Connection getConnection() {
        return connection;
    }


    @Override
    public void close() throws Exception {

    }
}



