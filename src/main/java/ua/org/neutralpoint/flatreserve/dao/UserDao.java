package ua.org.neutralpoint.flatreserve.dao;


import java.sql.ResultSet;

public interface UserDao {
    public ResultSet GetUserInfo(String id) throws Exception;
    public void addAvatarUrlPath(String avatar_url_path) throws Exception;
    public void addAvatarPicture(String avatar_url_path) throws Exception;
    public void addUsernameAndPassword(String username, String password) throws Exception;
}
