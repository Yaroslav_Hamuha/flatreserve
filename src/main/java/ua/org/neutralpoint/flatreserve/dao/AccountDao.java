package ua.org.neutralpoint.flatreserve.dao;


import java.sql.ResultSet;
import java.util.List;

public interface AccountDao {

    public ResultSet getUserAccount(int id);
    public List <ResultSet> showUserInfo (ResultSet rs);

}
