package ua.org.neutralpoint.flatreserve;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserValidatorServlet extends HttpServlet {
    private static final Map <String, UserAuthentication> users = getUsers();

    /**
     * Creates valid users
     *
     * This User Map could be users returned from a database
     * or a simple select with the user.name
     *
     * @return a Map of valid users
     */
    private static Map<String, UserAuthentication> getUsers() {
        Map<String, UserAuthentication> users = new HashMap<>();

        UserAuthentication userOne = new UserAuthentication("user","user");
        UserAuthentication userTwo = new UserAuthentication("admin","admin");

        users.put(userOne.getName(), userOne);
        users.put(userTwo.getName(), userTwo);

        return users;
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher rd;
        String name = req.getParameter("name");
        String password = req.getParameter("password");

        UserAuthentication user = validateLogin(name, password);

        if (user == null){
            rd = req.getRequestDispatcher("/loginError.jsp");
        }
        else{
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            rd = req.getRequestDispatcher("/loginSuccess.jsp");
        }

        rd.forward(req, res);
    }

    /**
     * Validate the entered data
     *
     * If there is no valid data, the method will return null
     *
     * @param name given at the jsp
     * @param password given at the jsp
     * @return a user if one was found and validated
     */
    private UserAuthentication validateLogin(String name, String password) {
        // All parameters must be valid
        if (name == null || password == null){
            return null;
        }

        // Get a user by key
        UserAuthentication user = users.get(name);

        if (user == null){
            return null;
        }

        // Check if the password is valid
        if (!user.getPassword().equals(password.trim())){
            return null;
        }

        return user;
    }
}