package ua.org.neutralpoint.flatreserve;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ua.org.neutralpoint.flatreserve.dao.JdbcConnection;
import ua.org.neutralpoint.flatreserve.dao.impl.UserDaoJDBCImpl;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public class GetUserInfoTag extends TagSupport {

    private String id;

    public void setId(String id) {
        this.id = id;
    }

    Logger log = LogManager.getLogger(GetUserInfoTag.class.getName());


    public int doStartTag()throws JspException{

        JspWriter out = pageContext.getOut();
        ResultSet rs = null;
        UserDaoJDBCImpl user = new UserDaoJDBCImpl();

            try {
                rs = user.GetUserInfo(id);
            } catch (Exception e) {
                log.error("GetUserInfoTag - setId - resultset error "+e);
            }

            try {

            while (rs.next())
            {
                int account_value = rs.getInt("account_value");
                String account_data = rs.getString("account_data");
                out.print("Account value of registered user: " + account_value + "<br>");
                out.print("Account data of registered user: " + account_data + "<br>");
            }
        } catch (SQLException|IOException e) {
            //e.printStackTrace();
            log.error("Error when trying to /'Account value of registered user/'"+e);
        }

        return SKIP_BODY;
    }
}


