package ua.org.neutralpoint.flatreserve.servlets;

import ua.org.neutralpoint.flatreserve.dao.impl.LandlordDaoJDBCImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class AddLandlordServlet extends HttpServlet {

    Logger log = LogManager.getLogger(AddLandlordServlet.class.getName());

    public void init() throws ServletException {
        //data cache
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        request.setCharacterEncoding("UTF-8");
        String landlordName = (String) request.getParameter("name");
        String landlordPhone = (String) request.getParameter("phone");
        String landlordEmail = (String) request.getParameter("email");

        LandlordDaoJDBCImpl landlord = new LandlordDaoJDBCImpl();
        try {
            landlord.addLandlord(landlordName, landlordPhone, landlordEmail);
        } catch (Exception e) {
            log.error("landlord.addLandlord"+e);
        }

    }
    public void destroy() {
    }
}





