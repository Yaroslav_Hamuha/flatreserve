package ua.org.neutralpoint.flatreserve.servlets;

import ua.org.neutralpoint.flatreserve.dao.impl.ApartmentDaoJDBCImpl;
import ua.org.neutralpoint.flatreserve.dao.impl.TenantDaoJDBCImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet(name="DeleteApartmentServlet", urlPatterns = "/deleteapartment")
public class DeleteApartmentServlet extends HttpServlet {

    Logger log = LogManager.getLogger(DeleteApartmentServlet.class.getName());


    public void init() throws ServletException {
        //data cache
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        request.setCharacterEncoding("UTF-8");
        int apartmentId = Integer.parseInt(request.getParameter("id"));

        ApartmentDaoJDBCImpl apartment = new ApartmentDaoJDBCImpl();
        try {
            apartment.deleteApartment(apartmentId);
        } catch (Exception e) {
            log.error("apartment.deleteApartment error"+e);
        }

    }
    public void destroy() {
    }

}





