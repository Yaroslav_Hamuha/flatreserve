package ua.org.neutralpoint.flatreserve.servlets;

import ua.org.neutralpoint.flatreserve.dao.impl.ApartmentDaoJDBCImpl;
import ua.org.neutralpoint.flatreserve.dao.impl.TenantDaoJDBCImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@WebServlet(name="AddApartmentServlet", urlPatterns = "/addapartment")
public class AddApartmentServlet extends HttpServlet {

//    public void init() throws ServletException {
//        //data cache
//    }

    Logger log = LogManager.getLogger(AddApartmentServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        request.setCharacterEncoding("UTF-8");
        int apartmentLandlordId = Integer.parseInt(request.getParameter("landlord_id"));
        String apartmentAddress = (String) request.getParameter("address");
        int apartmentPrice = Integer.parseInt(request.getParameter("price"));
        String apartmentPeriod = (String) request.getParameter("period");
        String apartmentStatus = (String) request.getParameter("status");

        ApartmentDaoJDBCImpl apartment = new ApartmentDaoJDBCImpl();
        try {
            apartment.addApartment(apartmentLandlordId, apartmentAddress, apartmentPrice,apartmentPeriod, apartmentStatus);
        } catch (Exception e) {
            log.error("apartment.addApartment"+e);
        }

    }
//    public void destroy() {
//
//    }


}





