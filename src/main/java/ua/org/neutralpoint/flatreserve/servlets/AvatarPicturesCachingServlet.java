package ua.org.neutralpoint.flatreserve.servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.org.neutralpoint.flatreserve.AvatarPicturesCaching;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;


@WebServlet("/avatarpicturescaching")
@MultipartConfig

public class AvatarPicturesCachingServlet extends HttpServlet {

    Logger log = LogManager.getLogger(AvatarPicturesCachingServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        AvatarPicturesCaching apc = AvatarPicturesCaching.getInstance();

        for (Part part : request.getParts()) {
            String fileNameAndExtension = extractFileNameAndExtension(part);

            if (apc.isAvatarPictureSmall(fileNameAndExtension)) {
                try {
                    apc.saveAvatarPictureToDB(fileNameAndExtension);
                } catch (Exception e) {
                    log.error("saveAvatarPictureToDB error "+e);
                }
                System.out.println("saveToDB completed");
            } else {
                try {
                    apc.saveToHDD(fileNameAndExtension);
                } catch (Exception e) {
                    log.error("saveToHDD error "+e);
                }
                System.out.println("saveToHDD completed");
            }
        }
    }

    /**
     * Returns String that contain input file name only.
     * <p>
     * This method always returns only file name
     * as real format <fileName>.<ext>
     * @param  filePart  reference of full request POST
     * @return      String <fileName>.<ext>
     * @see         String
     */
    private String extractFileNameAndExtension(Part filePart) {
        String contentDisp = filePart.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if ((s.trim().endsWith(".jpg\"")) || (s.trim().endsWith(".png\"")) || (s.trim().endsWith(".bmp\"")) ||
                (s.trim().endsWith(".gif\""))){
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}









