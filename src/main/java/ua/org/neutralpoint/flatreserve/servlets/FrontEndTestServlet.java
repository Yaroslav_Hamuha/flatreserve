package ua.org.neutralpoint.flatreserve.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FrontEndTestServlet extends HttpServlet {

    Logger log = LogManager.getLogger(FrontEndTestServlet.class.getName());


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {



        req.setAttribute("name", "message from GetUserAccountServlet");
        req.setAttribute("name2", "message2 from GetUserAccountServlet");
        req.getRequestDispatcher("frontEndTest.jsp").forward(req, resp);

    }
}





