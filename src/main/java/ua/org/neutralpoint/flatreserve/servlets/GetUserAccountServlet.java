package ua.org.neutralpoint.flatreserve.servlets;

import ua.org.neutralpoint.flatreserve.dao.impl.AccountDaoJDBCImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@WebServlet(name="GetUserAccountServlet", urlPatterns = "/getuseraccount")
public class GetUserAccountServlet extends HttpServlet {

    Logger log = LogManager.getLogger(GetUserAccountServlet.class.getName());


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("user_id"));

        //AccountDaoJDBCImpl userAccount = new AccountDaoJDBCImpl();

        // the servlet will shouldn't be knew about JDBC
        // move this method to AccountDaoJDBCImpl
        //ResultSet userAccountResult = userAccount.getUserAccount(id);

        // invoke method for ResultSet handling
        // method will return a List of GetUserInfoTag data

        // ResultSet result = userAccount.showUserInfo(userAccountResult);

        // result !!

        int result = id;

       // no direct output to jsp
       // request.setAttribute("name", result);
       //request.getRequestDispatcher("getUserAccountOutput.jsp").forward(request, response);

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
