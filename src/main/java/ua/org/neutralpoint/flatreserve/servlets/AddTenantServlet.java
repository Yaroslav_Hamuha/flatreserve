package ua.org.neutralpoint.flatreserve.servlets;

import ua.org.neutralpoint.flatreserve.dao.impl.TenantDaoJDBCImpl;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class AddTenantServlet extends HttpServlet {

    Logger log = LogManager.getLogger(AddTenantServlet.class.getName());

    public void init() throws ServletException {
        //data cache
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String tenantName = (String) request.getParameter("name");
        String tenantPhone = (String) request.getParameter("phone");
        String tenantEmail = (String) request.getParameter("email");
            TenantDaoJDBCImpl tenant = new TenantDaoJDBCImpl();
        try {
            tenant.addTenant(tenantName,tenantPhone,tenantEmail);
        } catch (Exception e) {
            log.error("tenant.addTenant error"+e);
        }
    }
    public void destroy() {
    }
}





