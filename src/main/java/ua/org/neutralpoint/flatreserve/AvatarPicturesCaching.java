package ua.org.neutralpoint.flatreserve;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.org.neutralpoint.flatreserve.dao.impl.UserDaoJDBCImpl;

import java.io.*;
import java.util.*;

public final class AvatarPicturesCaching {

    public final int FILE_SIZE_TRESHOLD = 100000;

    Logger log = LogManager.getLogger(FlatReserveCaching.class.getName());

    /**
     * Set class as Singleton
     */
    private static AvatarPicturesCaching global_cache;
    private AvatarPicturesCaching(){};
    public static synchronized AvatarPicturesCaching getInstance() {
        if (global_cache == null)
            global_cache = new AvatarPicturesCaching();
        return global_cache;
    }

    /**
     * Returns String with randomly elements
     * <p>
     * This method generate randomly string for method: getFilePath
     * and create fully fileNameAndExtension
     * @param  length  pick up to max length of symbols generating
     * @return      result
     * @see         String
     */
    public static String getRandomString(int length) {
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }
        return result.toString();
    }
    /**
     * Returns String with full fileNameAndExtension
     * <p>
     * This method return full fileNameAndExtension
     * @return      fileNameAndExtension
     * @see         String
     */
    public String getFilePath(){
        Scanner	stdInScanner = new Scanner(System.in);
        System.out.println("enter fileName:");
        String fileName = stdInScanner.nextLine();
        System.out.println("enter format");
        String extension = stdInScanner.nextLine();
        String fileNameAndExtension	= fileName+"_"+getRandomString(8)+"."+extension;
        return fileNameAndExtension;
    }

    /**
     * isAvatarPictureSmall checking
     * <p>
     * This method return true when isAvatarPictureSmall
     * and false when !isAvatarPictureSmall
     * @return      boolean
     * @see         boolean
     */
    public boolean isAvatarPictureSmall(String fileForEqual) throws IOException {
        FileInputStream fin = null;
        String fullPathForEqual = "C://userImages//" + fileForEqual;

        try {
            fin = new FileInputStream(fullPathForEqual);
            System.out.println("file size:" + fin.available());
        } catch (FileNotFoundException e) {
            log.error("FileInputStream error" + e);
            fin.close();
        }

        return (((fin.available()) < FILE_SIZE_TRESHOLD) ? true : false);

    }

    /**
     * Method which saving picture to HDD and
     * duplicate url_path of avatar picture to database
     * <p>
     * This method providing save file to HDD and
     * saving url_path of avatar picture to DB
     * @param       fileNameAndExtensionToHDD
     * @return      void
     */
    public void saveToHDD(String fileNameAndExtensionToHDD) throws Exception {

        String fullPathForExistFile = "C://userImages//" + fileNameAndExtensionToHDD;
        String fullPathForSaveToHDD = "C://hdd//" + fileNameAndExtensionToHDD;

        try(FileInputStream fin=new FileInputStream(fullPathForExistFile);
            FileOutputStream fos=new FileOutputStream(fullPathForSaveToHDD))
            {
                byte[] buffer = new byte[fin.available()];
                fin.read(buffer, 0, buffer.length);
                fos.write(buffer, 0, buffer.length);
            } catch(IOException e){
                log.error("saveToHDD: FileInputStream FileOutputStream errors"+e);
            }

        UserDaoJDBCImpl userDAO = new UserDaoJDBCImpl();
        userDAO.addAvatarUrlPath(fullPathForSaveToHDD);
    }

    /**
     * Method saving binary stream pictures to data base
     * and duplicate this binary data stream to internal cache
     * <p>
     * This method generate randomly string for method: getFilePath
     * and create fully fileNameAndExtension
     * @param       fileNameAndExtension  pick up to max length of symbols generating
     * @return      void
     */
    public void saveAvatarPictureToDB(String fileNameAndExtension) throws Exception {

        /**
         * Saving to DB
         */
        String fullPathForExistFile = "C://userImages//" + fileNameAndExtension;
        UserDaoJDBCImpl userDAO = new UserDaoJDBCImpl();
        userDAO.addAvatarPicture(fullPathForExistFile);

        /**
         * duplicate binary data to HashMap Cache
         */
        try (FileInputStream fin = new FileInputStream(fullPathForExistFile);) {
            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);

            Map<String, Object> global_cache = new HashMap<>();
            global_cache.put(fullPathForExistFile, buffer);

            //show Cache (HashMap) for test
            Iterator it = global_cache.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
            }
        } catch (IOException e) {
            log.error("saveAvatarPictureToDB: duplicate binary data to Collection Cache error" + e);
        }
    }


    public void showAvatarPicture(){
        // for small pictures: av_small.*
        // if unavailable in Cache - > * get from DB
        //                              * duplicate binary data to Cache

        // if available in cache -> * show
    }

    public void changingAvatarPicture(){
        // for big and small pictures ?

        // changing in DB;

        // duplicate changing in cache;

    }
}
