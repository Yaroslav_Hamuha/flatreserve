package ua.org.neutralpoint.flatreserve.entity;

import java.io.Serializable;

public class Apartment implements Serializable {
    private int id;
    private int landlord_id;
    private String address;
    private int price;
    private String period;
    private String status;

    public Apartment() {}
    public Apartment (int id, int landlord_id, String address, int price,
                   String period, String status) {
        this.id = id;
        this.landlord_id = landlord_id;
        this.address = address;
        this.price = price;
        this.period = period;
        this.status = status;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getLandlord_id() {
        return landlord_id;
    }
    public void setLandlord_id(int landlord_id) {
        this.landlord_id = landlord_id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String author) {
        this.address = author;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
