package ua.org.neutralpoint.flatreserve.entity;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String username;
    private String password;
    private String avatar;
    private String end_update;
    private String url_image;


    public User() {}
    public User (int id, String username, String password, String avatar,
                   String end_update, String url_image, String account_info) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.avatar = avatar;
        this.username = end_update;
        this.password = url_image;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEnd_update() {
        return end_update;
    }
    public void setEnd_update(String end_update) {
        this.end_update = end_update;
    }

    public String getUrl_image() {
        return url_image;
    }
    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

}
