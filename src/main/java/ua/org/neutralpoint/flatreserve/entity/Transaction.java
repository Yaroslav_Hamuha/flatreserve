package ua.org.neutralpoint.flatreserve.entity;

import java.io.Serializable;

public class Transaction implements Serializable {
    private int id;
    private int amount;
    private String rent_period;
    private int apartment_id;
    private int tenant_id;
    public Transaction() {}
    public Transaction (int id, int amount, String rent_period, int apartment_id,
                      int tenant_id) {
        this.id = id;
        this.amount = amount;
        this.rent_period = rent_period;
        this.apartment_id = apartment_id;
        this.tenant_id = tenant_id;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public String getRent_period() {
        return rent_period;
    }
    public void setRent_period(String author) {
        this.rent_period = author;
    }
    public int getApartment_id() {
        return apartment_id;
    }
    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }
    public int getTenant_id() {
        return tenant_id;
    }

    public void setTenant_id(int tenant_id) {
        this.tenant_id = tenant_id;
    }

}
