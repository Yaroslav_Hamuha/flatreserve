package ua.org.neutralpoint.flatreserve.entity;

import java.io.Serializable;

public class Account implements Serializable {
    private int id;
    private int user_id;
    private int account_value;

    public Account() {}
    public Account (int id, int user_id, int account_value) {
        this.id = id;
        this.user_id = user_id;
        this.account_value = account_value;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getAccount_value() {
        return account_value;
    }
    public void setAccount_value(int account_value) {
        this.account_value = account_value;
    }
}
