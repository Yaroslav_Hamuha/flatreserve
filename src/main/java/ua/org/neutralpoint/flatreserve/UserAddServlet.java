package ua.org.neutralpoint.flatreserve;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.org.neutralpoint.flatreserve.dao.impl.UserDaoJDBCImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAddServlet extends HttpServlet {

    Logger log = LogManager.getLogger(UserAddServlet.class.getName());

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher rd;
        String name = req.getParameter("nameNew");
        String password = req.getParameter("passwordNew");

        UserDaoJDBCImpl userDAO = new UserDaoJDBCImpl();
        try {
            userDAO.addUsernameAndPassword(name, password);
        } catch (Exception e) {
            log.error("Creating new user error"+e);
        }

        if ((name == null) || (password == null)) {
            rd = req.getRequestDispatcher("/createError.jsp");
        }
        else{
            rd = req.getRequestDispatcher("/createSuccess.jsp");
        }

        rd.forward(req, res);
    }

}